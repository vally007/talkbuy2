function loadScript(url) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    head.appendChild(script);
}

loadScript("https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js");

loadScript("https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js");

loadScript("https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js");



function addTable(nr) {
    //var nr = document.getElementById(id).value;
    var container = document.getElementById("about1");
    container.innerHTML = '';
    for (var i = 1; i <= nr; i++) {
        container.innerHTML += createTable(i, 1);
    }
    console.log("da");
}

function addLine(idTable, idLine) {
    var container = document.getElementById('tbody-' + idTable);
    container.innerHTML = '';
    for (var i = 1; i <= idLine; i++) {
        container.innerHTML += createLine(idTable, i);
    }
}

function deleteLine(id) {
    document.getElementById(id).remove();
}


function createTable(idTable, idLine) {
    var html = '' +
        '<table class="table table-striped table-bordered table-list" id="tab-' + idTable + '" style="border-style: outset; border-bottom: 6px outset red;" >\n' +
        '<thead>' +
        '<tr>' +
        '<td>' +
        'Titlu:' +
        '<input class="form-control" type="text" name="title2[]" value="" id="title1" style="width:200px; height:30px; margin-top:10px;">' +
        '</td>' +
        '<td>'+
        'Numar Specificatii:'+
            '<select name="nr_spec" class="form-control" style=" margin-top:10px;" onchange="addLine('+idTable+',this.value)">'+
            '<option value="0"></option>'+
            '<option value="1">1</option>'+
            '<option value="2">2</option>'+
            '<option value="3">3</option>'+
            '<option value="4">4</option>'+
            '<option value="5">5</option>'+
            '</select>'+
        '</td>'+
        '</tr>' +
        '</thead>' +
        '<thead>' +
        '<tr>' +
        '<td>Informatie</td>' +
        '<td>Raspuns</td>' +
        '</tr>' +
        '</thead>' +
        '<tbody id="tbody-' + idTable + '">' +
        '<tr id="' + idTable + '-tr-' + idLine + '">' +
        '<td>' +
        '<input class="form-control" type="text" name="info[]" id="info1" style="width:200px; height:30px;" >' +
        '</td>' +
        '<td>' +
        '<input class="form-control" type="text" name="raspuns[]" id="info1" style="width:200px; height:30px;" >' +
        '</td>' +
        '</tr>' +
        '</tbody>' +
        '</table>';
    return html;
}

function createLine(idTable, idLine) {
    var html = '' +
        '<tr id="' + idTable + '-tr-' + idLine + '">' +
        '<td>' +
        '<input class="form-control" type="text" name="info[]" id="info1" style="width:200px; height:30px;" >' +
        '</td>' +
        '<td>' +
        '<input class="form-control" type="text" name="raspuns[]" id="info1" style="width:200px; height:30px;"  >' +
        '</td>' +
        '</tr>';
    return html;
}
