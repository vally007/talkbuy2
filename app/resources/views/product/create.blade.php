


@extends('layouts/layout1')



@section('content')
<h1> Adauga</h1>

<div class="container">
    <form action="/adauga/produs/" method="POST" style="font-size: 30px;" enctype="multipart/form-data">
    {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Titlu:</label>
            <textarea class="form-control" name="title" id="title" cols="30" rows="2" ></textarea>
        </div>
        <div class="form-group">
            <label for="categories">Categorie:</label>
            <select class="form-control" name="category" id="categories">
             @foreach($categories as  $category)
            <option value="{{ $category->id }}" </option> {{$category->title}} </option>
             @endforeach
            </select>
        </div>
        <div class="form-inline"style="margin-bottom:10px;">
            <label for="price">Pret:</label>
            <input type="number" class="form-control" name="price" id="price" style="height:30px;" value="1" min="1" >
        </div>
        <div class="form-inline" style="margin-bottom:10px;">
            <label for="discount">Discount:</label>
            <input type="number" class="form-control" name="discount"  id="discount" style="height:30px;" value="1" min="1" >
        </div>
        <div class="form-inline" style="margin-bottom:10px;">
            <label for="quantity">Cantitate:</label>
            <input type="number" class="form-control" name="quantity"  id="quantity" style="height:30px;" value="1" min="1" >
        </div>
        <div class="form-group" style="margin-bottom:10px;">
            <label for="imgUpload1">Imagini:</label>
            <input type="file" class="form-control" name="imgUpload1" accept=".png, .jpg, .jpeg"  id="imgUpload1" style="height:50px;"  >
        </div>
        <div class="form-inline">
            <label for="nrSpec">Numar Specificatii:</label>
            <select class="form-control" name="nrSpec" id="nrSpec" onchange="addTable(this.value)">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
        <div class="form-group" style="margin-bottom:10px;">
            <label for="about1">Specificatii:</label>
            <div id="about1" class="container" style="margin-bottom:10px;">
                <table class="table table-striped table-bordered table-list" style="border-style: outset; border-bottom: 6px outset red;">
                    <thead>
                        <tr>
                            <td>
                                Titlu:
                                <input class="form-control" type="text" name="title2[]" value="" id="title1" style="width:200px; height:30px; margin-top:10px;">
                            </td>
                            <td>
                                Numar Specificatii:
                                <select name="nr_spec" class="form-control" style=" margin-top:10px;" onchange="addLine(1,this.value)" >
                                    <option value="0"></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </td>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <td>Informatie</td>
                            <td>Raspuns</td>
                        </tr>
                    </thead>
                    <tbody id="tbody-1">
                        <tr id="1-tr-1">
                            <td>
                                <input class="form-control" type="text" name="info1[]"  style="width:200px; height:30px;" >
                            </td>
                            <td>
                                <input class="form-control" type="text" name="raspuns1[]"  style="width:200px; height:30px;" >
                            </td>
                        </tr>


                    </tbody>
                </table>




            </div>


        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<br><br><br><br><br><br>
@endsection
