


@extends('layouts/layout1')



@section('content')


<div>
    <div class="container">
        <div class="row">
            <h1>Lista Produse</h1>
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col col-xs-6 text-right">
                                <a type="button" class="btn btn-sm btn-primary btn-create" href="/produs/adauga/" >Adauga</a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-bordered table-list">
                            <thead>
                                <tr>
                                    <th><em class="fa fa-cog"></em></th>
                                    <th class="hidden-xs">ID</th>
                                    <th>Id Produs</th>
                                    <th>Titlu</th>
                                    <th>Link</th>
                                    <th>Pret</th>
                                    <th>Discount</th>
                                    <th>Cantitate</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"></td>
                                    <td class="hidden-xs">0</td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control">
                                    </td>
                                </tr>
                                @foreach($products as $product)
                                <tr>
                                    <td align="center">
                                        <a class="btn btn-default" href={{'/produs/edit/'.$product->path}} >
                                            <em class="fa fa-pencil"></em>
                                        </a>
                                        <a class="btn btn-danger" onclick="">
                                            <em class="fa fa-trash"></em>
                                        </a>
                                    </td>
                                    <td class="hidden-xs">1</td>
                                    <td>{{$product->id}}</td>
                                    <td>{{$product->title}} </td>
                                    <td>{{$product->path}} </td>
                                    <td>{{$product->price}} Lei</td>
                                    @if($product->discount)
                                       <td align="center">{{$product->discount}} %</td>
                                    @else
                                       <td>Fara Discount</td>
                                    @endif
                                    <td>{{$product->quantity}} </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <div class="row">
                            <div class="col col-xs-4">Page 1 of 5
                            </div>
                            <div class="col col-xs-8">
                                <ul class="pagination hidden-xs pull-right">
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                </ul>
                                <ul class="pagination visible-xs pull-right">
                                    <li><a href="#">«</a></li>
                                    <li><a href="#">»</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<br>
<br>
<br>

@endsection
