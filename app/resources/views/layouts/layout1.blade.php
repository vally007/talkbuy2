
<!DOCTYPE html>
<html>
<head>
    <title>Cursuri Online</title>
    <link href="/display/public_html/css/app.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Brand -->
  <a class="navbar-brand" href="/">TalkBuy Admin Panel</a>

  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="/">Produse</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/categorii">Categorii</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/recenzii">Recenzii</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="/plati">Plati</a>
    </li>

    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
        Dropdown link
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Link 1</a>
        <a class="dropdown-item" href="#">Link 2</a>
        <a class="dropdown-item" href="#">Link 3</a>
      </div>
    </li>
  </ul>
</nav>

  @yield('content')
  <script src="/js/app.js"></script>
</body>
</html>
