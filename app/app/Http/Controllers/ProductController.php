<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Categories;

class ProductController extends Controller
{
    public function index()
    {
        $products =  Product::all();
        return view('product/index',
             [
                'products'=> $products
             ]
        );
    }

    public function create()
    {
        $categories = Categories::all();
        return view('product/create',
            [
                'categories'=>$categories
            ]
        );
    }
    public function store(Request $request)
    {
        $title = $request->title;
        $category = $request->category;
        $price = $request->price;
        $discount = $request->discount;
        $quantity = $request->quantity;
        $imagess = $request->imgUpload1;
        var_dump($imagess);


        $extension = $request->image->extension();
        $request->image->storeAs('/public/images', "test.".$extension);

        $product = new Product();
    }
}
