<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('display/public_html/', 'ProductController@index');


Route::get('display/public_html/produs/adauga/', 'ProductController@create');


//Route::get('display/public_html/adauga/produs/, 'ProductController@store' );

Route::post('display/public_html/adauga/produs/', 'ProductController@store');
